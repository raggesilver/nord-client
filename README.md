<div align="center">
    <h1>
        <img src="https://gitlab.com/raggesilver/nord-client/raw/master/data/icons/hicolor/scalable/apps/com.raggesilver.NordClient.svg" /> Nord Client
    </h1>
    <h4>A GTK GUI for NordVPN</h4>
    <p>
        <a href="https://gitlab.com/raggesilver/nord-client/pipelines">
            <img src="https://gitlab.com/raggesilver/nord-client/badges/master/pipeline.svg" alt="Build Status" />
        </a>
        <a href="https://www.patreon.com/raggesilver">
            <img src="https://img.shields.io/badge/patreon-donate-orange.svg?logo=patreon" alt="Support on Patreon" />
        </a>
    </p>
    <p>
        <a href="#install">Install</a> •
        <a href="#features">Features</a> •
        <a href="COPYING">License</a>
    </p>
</div>

One day project to help me use NordVPN on linux.

<div align="center">
    <img src="https://imgur.com/ZtuNwAe.png">
    <img src="https://imgur.com/kZe5w1y.png">
    <img src="https://imgur.com/FNMokaL.png">
</div>

## Features
- Connect to any country/city available
- Connect to the fastest location available
- Updates on status
- Disconnect

## Install

**Download**

[Flatpak](https://gitlab.com/raggesilver/nord-client/-/jobs/artifacts/master/file/nord-client.flatpak?job=deploy) • [Zip](https://gitlab.com/raggesilver/nord-client/-/jobs/artifacts/master/download?job=deploy)

*Note: these two links might not work if the latest pipeline failed/is still running*

## Compile

**Flatpak from source**

```bash
# Clone the repo
git clone https://gitlab.com/raggesilver/nord-client
# cd into the repo
cd nord-client
# Assuming you have both flatpak and flatpak-builder installed
sh test.sh
```

**Regular from source (unsupported)**

```bash
# Clone the repo
git clone --recursive https://gitlab.com/raggesilver/nord-client
# cd into the repo
cd nord-client
meson _build
ninja -C _build
# sudo
ninja -C _build install
```

*Note: Nord Client can be ran from GNOME Builder*
