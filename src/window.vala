/* window.vala
 *
 * Copyright 2020 Paulo Queiroz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace NordClient
{
  public bool is_flatpak()
  {
    return (FileUtils.test("/.flatpak-info", FileTest.EXISTS));
  }

  public bool spawn_command_line_sync(string c, out string o,
                                      out string e, out int r) throws Error
  {
    string cmd;
    bool   res;

    if (is_flatpak())
      cmd = "flatpak-spawn --host sh -c '%s'".printf(c);
    else
      cmd = c;
    res = Process.spawn_command_line_sync(cmd, out o, out e, out r);
    return (res);
  }

  /*
  ** SCLA stands for "spawn command line async". This class is a helper to run
  ** host commands on a thread and get the result through signals, not really
  ** "Vala async", but this is the best way I could find to run system commands
  ** that don't hang the UI.
  */

  class SCLA
  {
    public signal void finished(bool br, string o, string e, int s);
    public signal void error(GLib.Error e);
    public signal void _finally();

    private string o;
    private string e;
    private int s;
    private bool res;
    private GLib.Error err;

    public void run(string _cmd)
    {
      new Thread<bool>("spawn-cmd-async", () => {
        string cmd;

        if (is_flatpak())
          cmd = "flatpak-spawn --host sh -c '%s'".printf(_cmd);
        else
          cmd = _cmd;

        try
        {
          string[] spawn_env = {};
          string[] cmdv = null;

          Shell.parse_argv(cmd, out cmdv);

          foreach (string es in Environ.get())
            if (!es.has_prefix("G_MESSAGES_DEBUG"))
              spawn_env += es;

          spawn_env += "G_MESSAGES_DEBUG=false";
          this.res = Process.spawn_sync(null,
                                        cmdv,
                                        spawn_env,
                                        SpawnFlags.SEARCH_PATH,
                                        null,
                                        out this.o,
                                        out this.e,
                                        out this.s);
          Idle.add(this.emit_finished);
        }
        catch (Error e)
        {
          this.err = e;
          Idle.add(this.emit_error);
        }
        return (false);
      });
    }

    private bool emit_error()
    {
      this.error(this.err);
      this._finally();
      return (false);
    }

    private bool emit_finished()
    {
      this.finished(this.res, this.o, this.e, this.s);
      this._finally();
      return (false);
    }
  }
}

[GtkTemplate (ui = "/com/raggesilver/NordClient/connect-window.ui")]
public class NordClient.ConnectWindow : Gtk.Window
{
  [GtkChild] Gtk.ListStore countries_store;
  [GtkChild] Gtk.ListStore cities_store;
  [GtkChild] Gtk.Button connect_button;

  weak NordClient.Window win;

  public string? country { get; private set; default = null; }
  public string? city { get; private set; default = null; }

  public ConnectWindow(NordClient.Window paren)
  {
    this.win = paren;

    this.set_transient_for(paren);
    this.application = paren.application;

    this.notify["country"].connect(this.on_country_changed);
    this.notify.connect(this.on_notify);

    this.get_countries();
  }

  private void get_countries()
  {
    var r = new SCLA();

    // This command outputs { '-', '\13', ' ', ' ', '\13', rest... }
    r.run("nordvpn countries");

    r.finished.connect((res, sout, serr, status) => {
      if (!res || status != 0)
        return ;

      string s = sout.strip();
      string pre = "--  -";
      string[] countries = null;

      pre.data[1] = pre.data[4] = 13;
      if (s.has_prefix(pre))
        s = s.offset(5);

      this.countries_store.clear();
      this.country = null;
      this.city = null;

      countries = s.split(", ");
      foreach (string c in countries)
      {
        Gtk.TreeIter it;

        this.countries_store.append(out it);
        this.countries_store.set(it, 0, c, -1);
      }
    });
  }

  private void on_country_changed()
  {
    if (this.country == null)
      return ;

    var r = new SCLA();

    r.run("nordvpn cities %s".printf(this.country));

    r.finished.connect((res, sout, serr, status) => {
      if (!res || status != 0)
        return ;

      string s = sout.strip();
      message(s);
      string pre = "--  -";
      string[] cities = null;

      pre.data[1] = pre.data[4] = 13;
      if (s.has_prefix(pre))
        s = s.offset(5);

      this.cities_store.clear();
      this.city = null;

      cities = s.split(", ");
      foreach (string c in cities)
      {
        Gtk.TreeIter it;

        this.cities_store.append(out it);
        this.cities_store.set(it, 0, c, -1);
      }
    });
  }

  private void on_notify()
  {
    this.connect_button.sensitive = (this.country != null);
  }

  [GtkCallback]
  private void on_connect()
  {
    if (this.country == null)
    {
      this.connect_button.sensitive = false;
      return ;
    }

    string cmd = "nordvpn connect %s %s".printf(
      this.country,
      this.city ?? ""
    );

    var r = new SCLA();
    r.run(cmd);
    this.hide();
    r._finally.connect(() => {
      this.destroy();
    });
  }

  [GtkCallback]
  private void on_cancel()
  {
    this.destroy();
  }

  [GtkCallback]
  private void on_country_row_activated(Gtk.TreePath path,
                                        Gtk.TreeViewColumn column)
  {
    Gtk.TreeIter it;

    if (this.countries_store.get_iter_from_string(out it, path.to_string()))
    {
      string s;

      this.countries_store.get(it, 0, out s, -1);
      message("Selected country '%s'", s);
      this.country = s;
    }
  }

  [GtkCallback]
  private void on_city_row_activated(Gtk.TreePath path,
                                     Gtk.TreeViewColumn column)
  {
    Gtk.TreeIter it;

    if (this.cities_store.get_iter_from_string(out it, path.to_string()))
    {
      string s;

      this.cities_store.get(it, 0, out s, -1);
      message("Selected city '%s'", s);
      this.city = s;
    }
  }
}

[GtkTemplate (ui = "/com/raggesilver/NordClient/window.ui")]
public class NordClient.Window : Gtk.ApplicationWindow
{
	[GtkChild] Gtk.Stack stack;
	[GtkChild] Gtk.Label status_label;
	[GtkChild] Gtk.Label downloaded_label;
	[GtkChild] Gtk.Label uploaded_label;
	[GtkChild] Gtk.Label location_label;
	[GtkChild] Gtk.Label uptime_label;
	[GtkChild] Gtk.Label ip_label;

	private SimpleAction connect_action;
	private SimpleAction connect_fastest_action;
	private SimpleAction disconnect_action;
	private SimpleAction about_action;

	private Gtk.AboutDialog? about_window = null;

	public Window(Gtk.Application app)
	{
		Object(application: app);

		Gtk.IconTheme.get_default().append_search_path(
      @"$(NordClient.DATADIR)/nordclient/icons");

		var css_provider = new Gtk.CssProvider();
    css_provider.load_from_resource(
      "/com/raggesilver/NordClient/style.css");

    Gtk.StyleContext.add_provider_for_screen(
      Gdk.Screen.get_default(),
      css_provider,
      Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

    Gtk.Settings.get_default().gtk_application_prefer_dark_theme = true;

    this.add_actions();

		this.check_install();
	}

	private void add_actions()
	{
	  this.connect_fastest_action = new SimpleAction("connect_fastest", null);
	  this.connect_fastest_action.activate.connect(this.on_connect_fastest);
	  this.add_action(this.connect_fastest_action);

	  this.disconnect_action = new SimpleAction("disconnect", null);
	  this.disconnect_action.activate.connect(this.on_disconnect);
	  this.add_action(this.disconnect_action);

	  this.connect_action = new SimpleAction("connect", null);
	  this.connect_action.activate.connect(this.on_connect);
	  this.add_action(this.connect_action);

	  this.about_action = new SimpleAction("about", null);
	  this.about_action.activate.connect(this.on_about);
	  this.add_action(this.about_action);
	}

	private void check_install()
	{
	  string cmd = "command -v nordvpn";
	  SCLA r = new SCLA();

	  r.run(cmd);

	  r.finished.connect((res, sout, serr, status) => {
	    if (res && status == 0)
	    {
	      this.stack.set_visible_child_name("main");
	      this.loop();
	    }
	    else
	    {
	      debug("%s\n%s\n%s\n\nexit code %d", cmd, sout, serr, status);
	      this.stack.set_visible_child_name("not-installed");
	    }
	  });

	  r.error.connect((e) => {
	    warning(e.message);
	    this.stack.set_visible_child_name("not-installed");
	  });
	}

	private void loop()
	{
	  this.update_status(true);
	}

	public void update_status(bool repeat = false)
	{
	  string cmd = "nordvpn status";
	  SCLA r = new SCLA();

	  r.run(cmd);

	  r.finished.connect((res, sout, serr, status) => {
	    if (res && status == 0)
	      this.parse_status(sout.strip());
	    else
	      debug("%s\n%s\n%s\n\nexit code %d", cmd, sout, serr, status);
	  });

	  r.error.connect((e) => {
	    warning(e.message);
	  });

	  r._finally.connect(() => {
	    if (!repeat)
	      return;

	    Timeout.add(1000, () => {
	      this.update_status(true);
	      return (false);
	    });
	  });
	}

	private void parse_status(string s)
	{
	  var info = new HashTable<string, string>(str_hash, str_equal);
	  string[] parts = s.split("\n");
	  string[] sub_parts = null;

	  if (parts.length < 1)
	  {
	    warning("Weird output from 'nordvpn status' \"%s\"", s);
	    return ;
	  }
    foreach (string p in parts)
    {
      sub_parts = p.split(": ");
      if (sub_parts.length == 2)
        info.set(sub_parts[0], sub_parts[1]);
      else
        warning("Weird line '%s' from nordvpn status", p);
    }
    this.status_label.label = info.get("Status") ?? "-";
    this.set_connection_status();

    this.location_label.label = "<b>Location</b>: %s, %s".printf(
      info.get("City") ?? "Unknown",
      info.get("Country") ?? "Unknown"
    );

    this.uptime_label.label = "<b>Uptime</b>: %s".printf(
      info.get("Uptime") ?? "-"
    );

    this.ip_label.label = "<b>IP address</b>: %s".printf(
      info.get("Your new IP") ?? "-"
    );

    if (info.contains("Transfer"))
      this.update_transfer(info.get("Transfer"));
    else
    {
      this.downloaded_label.label = "-";
      this.uploaded_label.label = "-";
    }
	}

	private void set_connection_status()
	{
	  this.get_style_context().remove_class("Connected");
	  this.get_style_context().remove_class("Disconnected");
	  this.get_style_context().remove_class("Connecting");

	  switch (this.status_label.label)
	  {
	    case "Connected":
	      this.stack.visible_child.sensitive = true;
	      break;
	    case "Disconnected":
	    case "Connecting":
	      this.stack.visible_child.sensitive = false;
	      break;
	    default:
	      return ;
	  }

	  this.get_style_context().add_class(this.status_label.label);
	}

	private void update_transfer(string s)
	{
	  string[] parts = s.split(", ");
	  string[] sp = null;

	  if (parts.length != 2)
	    return ;
	  sp = parts[0].split(" ");
	  this.downloaded_label.label = "%s %s".printf(sp[0], sp[1]);

	  sp = parts[1].split(" ");
	  this.uploaded_label.label = "%s %s".printf(sp[0], sp[1]);
	}

	private void on_connect_fastest()
	{
	  string cmd = "nordvpn connect";
	  SCLA r = new SCLA();

    this.lock_all_actions();
	  r.run(cmd);
	  r._finally.connect(this.unlock_all_actions);
	}

	private void on_disconnect()
	{
	  string cmd = "nordvpn disconnect";
	  SCLA r = new SCLA();

    this.lock_all_actions();
	  r.run(cmd);
	  r._finally.connect(this.unlock_all_actions);
	}

	private void lock_all_actions()
	{
	  this.connect_action.set_enabled(false);
	  this.connect_fastest_action.set_enabled(false);
	  this.disconnect_action.set_enabled(false);
	}

	private void unlock_all_actions()
	{
	  this.connect_action.set_enabled(true);
	  this.connect_fastest_action.set_enabled(true);
	  this.disconnect_action.set_enabled(true);
	  this.update_status();
	}

	private void on_connect()
	{
	  this.lock_all_actions();
	  var w = new ConnectWindow(this);
	  w.show();
	  w.destroy.connect(() => {
	    this.unlock_all_actions();
	  });
	}

	private void on_about()
	{
	  if (this.about_window != null)
	  {
	    this.about_window.show();
	    return ;
	  }

	  var builder = new Gtk.Builder.from_resource(
      "/com/raggesilver/NordClient/about.ui");

    this.about_window = builder.get_object("about_window") as Gtk.AboutDialog;
    this.about_window.set_version(NordClient.VERSION);
    this.about_window.set_transient_for(this);
    this.about_window.show();
	}
}
